import Content from '../../../../contentBuild/Content';
import { useRouter } from 'next/router';
import NotFound from '../../../../components/NotFound';
import { Page } from './[page].js';

export const Subsection = (props) => {
    const router = useRouter();
    let subsectionContent = false;
    if(Object.keys(Content.$index$).indexOf(router.query.section) >= 0) {
        let sectionContent = Content.$index$[router.query.section]
        sectionContent.children.map((child) => {
            if(child.name === router.query.subsection.replace('-',' ')) {
                subsectionContent = child;
            }
        });
        if(subsectionContent) {
            if(subsectionContent.link &&
                subsectionContent.link !== `/docs/${router.query.section}/${router.query.subsection}`) {
                if(typeof window !== 'undefined') {
                    window.location.assign(subsectionContent.link);
                }
                return null;
            } else {
                return Page(props);
            }
        }
    }
    if(!subsectionContent) {
        return <NotFound></NotFound>;
    }
}

export async function getStaticProps(context) {
    return {
        props: {}
    };
}

export async function getStaticPaths() {
    let paths = Object.keys(Content).map((key) => {
        if (key.indexOf('$') > 0 && key.split('$').length === 2) {
            let section = key.split('$')[0];
            let subsection = key.split('$')[1].replace(/_/g,'-');
            return { params: { section, subsection } };
        }
        return false;
    }).filter((x) => !!x);
    return {
        paths,
        fallback: false
    };
}

export default Subsection;