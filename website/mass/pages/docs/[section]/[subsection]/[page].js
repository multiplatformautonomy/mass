import { useRouter } from 'next/router';
import NavBar from '../../../../components/NavBar';
import Footer from '../../../../components/Footer';
import Header from '../../../../components/Header';
import Content from '../../../../contentBuild/Content';
import ContentComponent from '../../../../components/ContentComponent';

export const Page = () => {
    const router = useRouter();
    let mdImport = router.query.section ? `${router.query.section}` : undefined;
    if(mdImport) {
        if (router.query.subsection) {
            mdImport += '/' + router.query.subsection;
            if (router.query.page) {
                mdImport += '/' + router.query.page;
            }
        }
    }
    return (
        <>
            <NavBar></NavBar>
            <Header background="#DBDBDB99"></Header>
            <ContentComponent path={mdImport}></ContentComponent>
            <Footer></Footer>
        </>
    )
};

export async function getStaticPaths() {
    let paths = Object.keys(Content).map((key) => {
        if (key.indexOf('$') > 0 && key.split('$').length === 3) {
            let section = key.split('$')[0];
            let subsection = key.split('$')[1].replace(/_/g,'-');
            let page = key.split('$')[2].replace(/_/g,'-');
            return { params: { section, subsection, page } };
        }
        return false;
    }).filter((x) => !!x);
    return {
        paths,
        fallback: false
    };
}

export async function getStaticProps(context) {
    return {
        props: {}
    };
}

export default Page;