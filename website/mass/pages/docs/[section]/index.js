import Content from '../../../contentBuild/Content';
import { useRouter } from 'next/router';
import NotFound from '../../../components/NotFound';
import { Page } from './[subsection]/[page].js';

export const Section = (props) => {
    const router = useRouter();
    if(Object.keys(Content.$index$).indexOf(router.query.section) >= 0) {
        let sectionContent = Content.$index$[router.query.section]
        if(sectionContent.home) {
            if(typeof window !== 'undefined') {
                window.location.assign(sectionContent.home);
            }
            return null;
        } else {
            return Page(props);
        }
    }
    else {
        return <NotFound></NotFound>;
    }
}

export async function getStaticProps(context) {
    return {
        props: {}
    };
}

export async function getStaticPaths() {
    let paths = Object.keys(Content).map((key) => {
        if (key.indexOf('$') === -1) {
            return { params: { section: key } };
        }
        return false;
    }).filter((x) => !!x);
    return {
        paths,
        fallback: false
    };
}

export default Section;