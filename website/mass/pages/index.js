import Head from '../components/Head'
import NavBar from '../components/NavBar';
import HomeHeader from '../components/HomeHeader';
import Footer from '../components/Footer';

export default function Home() {
  return (
    <>
      <Head>
        <title>MASS</title>
      </Head>
      
      <NavBar fixed="top"></NavBar>

      <HomeHeader></HomeHeader>

      <main>
      </main>

      <Footer>
      </Footer>

      <style jsx global>{`
        .footer {
          position: fixed;
          bottom: 0px;
          z-index: 3;
        }
      `}</style>
    </>
  )
}
