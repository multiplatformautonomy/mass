import App from 'next/app';

class MASSWebsite extends App {
    render() {
        return <this.props.Component {...this.props.pageProps} />
    }
    componentDidMount() {
        let links = document.getElementsByTagName('a');
        for(var i = 0; i < links.length; i++) {
            if(!links[i].hasAttribute('target')) {
                let target = (links[i].getAttribute('href').indexOf('/') === 0) ? '_self' : '_blank';
                links[i].setAttribute('target', target)
            }
        }
    }
}

export default MASSWebsite;