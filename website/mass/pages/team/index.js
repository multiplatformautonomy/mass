import Head from '../../components/Head'
import NavBar from '../../components/NavBar';
import Header from '../../components/Header';
import Footer from '../../components/Footer';

export default function Home() {
  return (
    <>
      <Head background="swarm.jpg">
        <title>MASS</title>
      </Head>
      
      <NavBar fixed="top"></NavBar>

      <Header background="#78787878"></Header>

      <div className="team-container">
        <div className="main">
          <div className="section">
            <h2>Current Team Members:</h2>
            <ul>
              <li>Sean Alderman</li>
              <li>Frazier Baker</li>
              <li>Kevin Cheng</li>
              <li>Alison Christensen</li>
              <li>Devon Current</li>
              <li>Kevin DeMarco</li>
              <li>Amy Gemme</li>
              <li>Prabhdip Gill</li>
              <li>James Lewis</li>
              <li>Mary Catharine Martin</li>
              <li>Sora Sung</li>
              <li>Collin Thomas</li>
              <li>Mya Tsang</li>
            </ul>
          </div>
          <div className="section">
            <h2>Project Leadership</h2>
            <ul>
              <li>Frazier N. Baker (Project Director)</li>
              <li>Kevin J. DeMarco (Principal Investigator)</li>
            </ul>
          </div>
        </div>
      </div>

      <Footer>
      </Footer>

      <style jsx global>{`
        footer {
          position: fixed;
          bottom: 0px;
          z-index: 3;
        }
        .team-container {
          height: 100vh;
          width: 100vw;
          display: grid;
          grid-template-rows: 130px 1fr 60px;
        }
        .main {
          position: relative;
          z-index: 3;
          background: #FFFFFFBB;
          margin-left: auto;
          margin-right: auto;
          width: 320px;
          padding: 10px;
          grid-row: 2;
          overflow-y: auto;
        }
        .main h2 {
          font-size: 16pt;
        }
      `}</style>
    </>
  )
}
