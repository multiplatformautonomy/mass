import Content from "../contentBuild/Content";
import { ListGroup, ListGroupItem, Button } from 'react-bootstrap'
import NoSSR from "./NoSSR";

const startState = (typeof window !== 'undefined') ? () => {
    if(window.innerWidth <= 600) {
        return 'hidden'; // mobile
    }
    return 'shown';
} : () => { return ''; };

export class ContentListGroup extends React.Component {
    render() {
        let navItems = [];
        Object.keys(Content.$index$).map((section, i) => {
            let title = Content.$index$[section].sectionTitle
            let children = Content.$index$[section].children;
            let href = `/docs/${section}`;
            let active = false;
            let activeChild = false;
            if(typeof window !== 'undefined') {
                if(href === window.location.pathname) {
                    active = true;
                } else if(window.location.pathname.indexOf(href) >= 0) {
                    activeChild = true;
                }
            }
            navItems.push(
                <ListGroupItem key={`list-group-lvl1-${i}-${title}`}
                    className="list-group-lvl1"
                    action
                    variant={ active ? "success" : (activeChild ? "primary" : "white") }
                    href={href}>
                    {title}
                </ListGroupItem>
            );
            children.forEach((page, index) => {
                active = false;
                activeChild = false;
                href = page.link
                if(typeof window !== 'undefined') {
                    if(href === window.location.pathname) {
                        active = true;
                    } else if(window.location.pathname.indexOf(href) >= 0) {
                        activeChild = true;
                    }
                }
                navItems.push(
                    <ListGroupItem key={`list-group-lvl2-${i}-${index}-${page.title}`}
                        className="list-group-lvl2"
                        action
                        variant={ active ? "success" : (activeChild ? "primary" : "white") }
                        href={`${page.link}`}>
                        {page.title}
                    </ListGroupItem>
                );
                if(page.children) {
                    if(page.children.length > 0) {
                        page.children.forEach((child, ind) => {
                            active = false;
                            href = child.link
                            if(typeof window !== 'undefined') {
                                if(href === window.location.pathname) {
                                    active = true;
                                }
                            }
                            navItems.push(
                                <ListGroupItem key={`list-group-lvl3-${i}-${index}-${child.title}`}
                                    className="list-group-lvl3"
                                    action
                                    variant={ active ? "success" : (activeChild ? "primary" : "white") }
                                    href={`${child.link}`}>
                                    {child.title}
                                </ListGroupItem>
                            );
                        });
                        
                    }
                }
            })
        })
        return (<NoSSR>
            <div className="container">
                <div className="main">
                    <ListGroup id='doc-nav-list-group' className={`flex-column ${startState()}`}>
                        {navItems}
                    </ListGroup>
                </div>
                <div className="expand" onClick={() => {
                    let current = document.getElementById('doc-nav-list-group').className;
                    if(current.indexOf("hidden") >= 0) {
                        document.getElementById('doc-nav-list-group').className = "flex-column shown";
                    } else {
                        document.getElementById('doc-nav-list-group').className = "flex-column hidden";
                    }
                }}>&gt;<br/>&lt;</div>
                <style jsx>{`
                    .container {
                        width: 100%;
                        height: 100%;
                        display: flex;
                        justify-content: flex-start;
                        align-items: stretch;
                        padding-right: 0px;
                        padding-left: 0px;
                    }
                    .list-group {
                        width: 100%;
                    }
                    .expand { display: none };
                    @media only screen and (max-width: 600px) {
                        .expand {
                            width: 10px;
                            font-size: 10pt;
                            background: #021D49;
                            color: #CCCCCC;
                            display: flex;
                            flex-direction: column;
                            justify-content: center;
                        }
                    }
                    .main {
                        background: white;
                    }
                    .shown {
                        width: 100%;
                    }
                `}</style>
                <style jsx global>{`
                    .list-group-lvl3 {
                        font-size: 10pt;
                        text-indent: 25px;
                    }
                    .list-group-lvl2 {
                        font-weight: 600;
                        font-size: 12pt;
                        text-indent: 10px;
                    }
                    .list-group-lvl1 {
                        font-weight: 800;
                        font-size: 14pt;
                    }
                    .hidden {
                        display: flex;
                    }
                    @media only screen and (max-width: 600px) {
                        .hidden {
                            display: none;
                        }
                    }
                `}</style>
            </div>
            </NoSSR>
        );
    };
}

export default ContentListGroup;