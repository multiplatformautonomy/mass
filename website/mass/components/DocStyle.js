import hljs from 'highlight.js';
import cheerio from 'cheerio';

const styleCode = (html) => {
    hljs.configure({

    })
    let dom$ = cheerio.load(html);
    let pres = dom$('pre').each((_i, preNode) => {
        let pre = dom$(preNode);
        if(pre.children('code').text().length > 0) {
            let code = pre.children('code').text();
            let highlighted = hljs.highlightAuto(code).value;
            dom$(preNode).html(`${highlighted}`);
        }
    });
    return dom$.html();
}

export const DocStyle = ({html}) => {
    return <>
        <div dangerouslySetInnerHTML={{
            __html: styleCode(html)
        }} className="md-content">
        </div>
        <style jsx global>{`
            .md-content {
                grid-column: 3;
                grid-row: 1;
                text-align: left;
                vertical-align: top;
                font-family: Arial, Helvetica, sans-serif;
                text-align: left;
                background: #FFFFFFCC;
                padding: 20px;
                width: 100%;
            }
            h1, h2, h3, h4, h5, h6, p {
                padding-top: 10px;
                text-align: left
            }
            h1, h2, h3, h4, h5 {
                font-weight: bold;
            }
            h6 {
                font-weight: normal;
            }
            .md-content a {
                color: #333333;
                text-decoration: dashed underline;
                padding-right: 0px;
            }
            .md-content h1 a, .md-content h2 a, .md-content h3 a, .md-content h4 a, .md-content h5 a, .md-content h6 a {
                text-decoration: none;
            }
            .md-content a:hover {
                color: #555555;
                text-decoration: underline;
                padding-right: 12px;
            }
            .md-content a:after {
                display: inline-block;
                content: ' ';
                background: url(/images/link.png);
                background-size: contain;
                width: 10px;
                height: 10px;
                margin: 1px;
            }
            .md-content h1 a:after, .md-content h2 a:after {
                display: inline-block;
                content: ' ';
                background: url(/images/link.png);
                background-size: contain;
                width: 20px;
                height: 20px;
                margin: 1px;
            }
            .md-content h3 a:after, .md-content h4 a:after {
                display: inline-block;
                content: ' ';
                background: url(/images/link.png);
                background-size: contain;
                width: 15px;
                height: 15px;
                margin: 1px;
            }
            .md-content a:hover:after {
                content: none;d
            }
            .md-content blockquote {
                padding: 10px;
                padding-left: 30px;
                font-style: italic;
                background: #00000022;
            }
            .md-content blockquote p {
                padding: 0px;
                margin: 0px;
            }
            .md-content code {
                font-family: Consolas, monospace;
            }
            .md-content pre {
                padding: 10px;
                background: #333333;
                color: #DDDDDD;
                font-family: Consolas, monospace;
            }
            .md-content img {
                display: block;
                width: 85%;
                max-width: 600px;
                margin: 5px;
                padding: 0px;
            }
            .md-content table {
                margin: 10px;
            }
            .md-content tr {
                border: 1px solid  #999;
            }
            .md-content td {
                border: 1px solid  #999;
                padding: 5px;
            }
            .md-content th {
                border: 1px solid  #999;
                text-align: center;
                padding: 5px;
            }
            .md-content th:hover, .md-content td:hover {
                background: #EEF9FF;
            }
        `}</style>
    </>
};

export default DocStyle