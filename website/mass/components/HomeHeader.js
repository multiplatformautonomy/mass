import Header from './Header';

export class HomeHeader extends Header {
    constructor(props) {
        super(props);
        this.adjOptions = [
            "accessible",
            "easy",
            "fun",
            "open",
            "better",
            "intuitive"
        ];
        this.state = { adj: this.adjOptions[0] };
        setTimeout(this.changeAdj.bind(this), 2000);
    }
    changeAdj() {
        let current = this.adjOptions.indexOf(this.state.adj);
        current = (current + 1) % this.adjOptions.length;
        this.setState({ adj: this.adjOptions[current] });
        setTimeout(this.changeAdj.bind(this), 2000);
    }
    render() {
        let background = super.render();
        return <>
            <div id="mass-header-title">
                <h1>Multiplatform Autonomy<br/>Simulation Suite</h1>
                <span>Making autonomy development<br/>{this.state.adj}</span>
                <style jsx>{`
                    #mass-header-title {
                        height: 100vh;
                        width: 100vw;
                        position: absolute;
                        z-index: 1;
                        top: 0px;
                        display: flex;
                        align-items: center;
                        justify-content: space-around;
                        flex-direction: column;
                        font-family: monospace;
                        text-align: center;
                    }
                    span {
                        font-size: 16pt;
                    }
                `}</style>
            </div>
            {background}
        </>;
    }
}

export default HomeHeader;