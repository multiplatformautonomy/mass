import Content from '../contentBuild/Content';
import NotFound from './NotFound';
import Head from './Head';
import ContentListGroup from './ContentListGroup';
import DocStyle from './DocStyle';

export const ContentComponent = (props) => {
    let name = props.path;
    if(name) {
        name = name.replace(/[^a-zA-Z0-9_\/]/g, '_')
        name = name.replace(/\//g,'$');
        if (Object.keys(Content).indexOf(name) >= 0) {
            let html = Content[name].html;
            let meta = Content[name].meta;
            return (
                <>
                <Head background="paper.jpg">
                    <title>MASS {meta.title ? `- ${meta.title}` : ''}</title>
                </Head>
                <div className="md-content-parent">
                    <DocStyle html={html}></DocStyle>
                    <div className="md-sidebar">
                        <ContentListGroup></ContentListGroup>
                    </div>

                    <style jsx>{`
                        .md-sidebar {
                            display: block;
                            grid-column: 1;
                            grid-row: 1;
                        }
                        .md-content-parent {
                            width: 100vw;
                            min-height: 100vh;
                            display: grid;
                            grid-template-columns: max-content 20px 2.5fr 20px;
                            grid-template-rows: 100%;
                            padding-top: 0px;
                            overflow-y: scroll;
                            position: relative;
                            top: 0px;
                            z-index: 9;
                        }
                        @media only screen and (max-width: 600px) {
                            .md-content-parent {
                                grid-template-columns: 10px; 10px; auto 10px;
                            }
                            .md-sidebar {
                                position: relative;
                                left: 0px;
                                z-index: 10;
                            }
                        }
                    `}</style>
                </div>
                </>
            );
        } else {
            return <NotFound></NotFound>
        }
    }   
    return <NotFound></NotFound>
}

export default ContentComponent;