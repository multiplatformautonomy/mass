import React from 'react'
export class Header extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        particlesJS.load("mass-header", '/data/particles.json')
    }
    render() {
        return (
            <div id="mass-header">
                <style jsx>{`
                    #mass-header {
                        height: 100vh;
                        width: 100vw;
                        position:absolute;
                        z-index: 2;
                        top: 0px;
                        background-color: ${this.props.background ? this.props.background : '#44444455'};
                    }
                `}</style>
            </div>
        );
    }
}

export default Header;