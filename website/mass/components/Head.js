import BaseHead from 'next/head';

export const Head = ({children, background}) => {
    if(!background) {
        background = 'quadcopter.jpg'
    }
    return <BaseHead>
        {children}
        <link rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossOrigin="anonymous"
        />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.2/styles/a11y-dark.min.css" crossOrigin="anonymous"></link>
        <script src="/scripts/particles.min.js"></script>
        <link rel="icon" href="/favicon.ico" />
        <style>{`
            body {
                background: url('/images/${background}');
                background-position: center;
                background-size: cover;
                background-attachment: fixed;
            }
            #__next {
                width: 100vw;
                height: 100vh;
            }
        `}
        </style>
    </BaseHead>
}

export default Head;