import DefaultErrorPage from 'next/error'
import Head from '../components/Head';

export const NotFound = (props) => {
    return <>
        <Head>
            <meta name="robots" content="noindex" />
        </Head>
        <DefaultErrorPage statusCode={404}></DefaultErrorPage>
    </>
};

export default NotFound;