
export const Footer = () => {
    return (
        <footer className="footer">
            <div>&copy; Georgia Tech Research Institute 2020.  <a style={{color: "#444444", textDecoration: "dashed underline", fontWeight: "bold"}} href="/docs/mass/licensing">Licensing</a>
            <div className="tiny">This material is based on research sponsored by the Air Force Research Laboratory under Subaward number FA8650-19-2-6983. The U.S. Government is authorized to reproduce and distribute reprints for Governmental purposes notwithstanding any copyright notation thereon.  The views and conclusions contained herein are those of the authors and should not be interpreted as necessarily representing the official policies or endorsements, either expressed or implied, of the Air Force Research Laboratory or the U.S. Government.</div>
            </div>
            <style jsx>{`
                .footer {
                    font-size: 8pt;
                    width: 100vw;
                    height: 55px;
                    text-align: center;
                    word-wrap: normal;
                    display: flex;
                    align-items: center;
                }
                .tiny {
                    font-size: 6pt;
                }
                @media screen and (max-width: 600px) {
                    .tiny {
                        font-size: 4pt;
                    }
                    .footer {
                        font-size: 5pt;
                    }
                }
            `}</style>
        </footer>
    );
}

export default Footer;