import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import DownloadList from '../data/downloads.json';

export const NavBar = ({fixed}) => {
    let tools = DownloadList.map((tool, index) => {
        return <NavDropdown.Item key={`tools-item-${tool.title}-${index}`} href={tool.link}>&emsp;{tool.title}</NavDropdown.Item>;
    });
    return (
        <>
            <Navbar fixed={fixed}>
                <Navbar.Brand href="/"><img id="logo" src="/images/MASS_1024.png" alt="Multiplatform Autonomy Simulation Suite logo" /></Navbar.Brand>
                <Nav className = "full">
                    <Nav.Item>
                        <Nav.Link href="/">Home</Nav.Link>
                    </Nav.Item>
                    <NavDropdown title="Downloads">
                        {tools}
                    </NavDropdown>
                    <NavDropdown title="Documentation">
                        <NavDropdown.Item href="/docs/mass">&emsp;MASS</NavDropdown.Item>
                        <NavDropdown.Item href="/docs/sss">&emsp;SSS</NavDropdown.Item>
                        <NavDropdown.Item href="/docs/vscrimmage">&emsp;Virtualized SCRIMMAGE</NavDropdown.Item>
                        <NavDropdown.Item href="/docs/demos">&emsp;Demonstration Code</NavDropdown.Item>
                        <NavDropdown.Item href="https://scrimmagesim.org">&emsp;SCRIMMAGE</NavDropdown.Item>
                    </NavDropdown>
                    <Nav.Item>
                        <Nav.Link href="https://gitlab.com/multiplatformautonomy">Code</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link href="https://multiplatformautonomy.slack.com">User Group</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link href="/team">Team</Nav.Link>
                    </Nav.Item>
                </Nav>
                <Nav className="partial">
                    <NavDropdown drop="left" title={<span className="hamburger">&#8801;</span>}>
                        <NavDropdown.Item href="/"><strong>Home</strong></NavDropdown.Item>
                        <NavDropdown.Divider></NavDropdown.Divider>
                        <NavDropdown.Item>
                            <strong>Downloads</strong>
                        </NavDropdown.Item>
                        {tools}
                        <NavDropdown.Divider></NavDropdown.Divider>
                        <NavDropdown.Item>
                            <strong>Documentation</strong>
                        </NavDropdown.Item>
                        <NavDropdown.Item href="/docs/mass/">&emsp;MASS</NavDropdown.Item>
                        <NavDropdown.Item href="/docs/sss">&emsp;SSS</NavDropdown.Item>
                        <NavDropdown.Item href="/docs/vscrimmage">&emsp;vSCRIMMAGE</NavDropdown.Item>
                        <NavDropdown.Item href="/docs/demos">&emsp;Demo Code</NavDropdown.Item>
                        <NavDropdown.Item href="https://scrimmagesim.org">&emsp;SCRIMMAGE</NavDropdown.Item>
                        <NavDropdown.Divider></NavDropdown.Divider>
                        <NavDropdown.Item href="/team"><strong>Team</strong></NavDropdown.Item>
                        <NavDropdown.Item href="https://gitlab.com/multiplatformautonomy"><strong>Code</strong></NavDropdown.Item>
                        <NavDropdown.Item href="https://multiplatformautonomy.slack.com"><strong>User Group</strong></NavDropdown.Item>
                    </NavDropdown>
                </Nav>
            </Navbar>
            <style jsx>{`
                #logo {
                    width: 100px;
                    height: auto;
                }
            `}</style>
            <style jsx global>{`
                .navbar {
                    z-index: 10;
                }
                .partial {
                    display: none;
                    position: absolute;
                    right: 20px;
                }
                .hamburger {
                    font-size: 40pt;
                }
                .partial .nav-link::before {
                    display: none;
                }
                .partial .dropdown-item {
                    font-size: 10.5pt;
                }
                @media only screen and (max-width: 600px) {
                    .partial {
                        display: flex;
                    }
                    .full {
                        display: none;
                    }
                }
            `}</style>
        </>
    );
}

export default NavBar;