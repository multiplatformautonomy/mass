{ "title": "Initial Setup" }
::meta::
# Virtualized SCRIMMAGE

The SCRIMMAGE build and installation process can be tricky, so the MASS team has automated this for you using Docker containers.

## Current Images

|                **Docker Image**               |                                                          **Description**                                                         |
|:---------------------------------------------:|:--------------------------------------------------------------------------------------------------------------------------------:|
| multiplatformautonomy/**vscrimmage-headless** | Lean image with a working installation of SCRIMMAGE and demonstration code                                                       |
| multiplatformautonomy/**vscrimmage-vnc**      | *vscrimmage-headless* image with an added desktop interface accessible through VNC and a browser-based noVNC server on port 6901 |

Please see https://hub.docker.com/u/multiplatformautonomy for the most up-to-date list of current images.

## Setup

### Installing Docker

Follow the instructions on [https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/) to install Docker for your platform.

Once you have installed docker on your machine, confirm the docker daemon is running by opening a terminal window and typing `docker ps`.  If the docker daemon is not running, you will get an error that looks like this:

> Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?

If you get this error, try restarting Docker desktop if on Windows or Mac OS X.  For linux, look at starting the docker daemon service (sometimes called `dockerd`).

### Pulling the Image

Select your desired docker image from the list above.  In a terminal, run the command `docker pull {docker_image_name}`.  Wait for the image to download.

Example:
```
# Download vSCRIMMAGE-VNC
docker pull multiplatformautonomy/vscrimmage-vnc
```
Renders an output similar to:
<pre>
Using default tag: latest
latest: Pulling from multiplatformautonomy/vscrimmage-vnc
7595c8c21622: Already exists
d13af8ca898f: Already exists
70799171ddba: Already exists
b6c12202c5ef: Already exists
e300a5f58498: Pull complete
b7b1c8638a62: Pull complete
61f40ecd844c: Pull complete
9588ff702d14: Pull complete
c4fe6a213eab: Pull complete
4ce54b080823: Pull complete
343e83e0b169: Pull complete
ac5c83d969ab: Pull complete
0d25d01353da: Pull complete
e75ee600c26d: Pull complete
18e5e196d3fd: Pull complete
6608c72c9bc6: Pull complete
bf8844fe475c: Pull complete
9854656f2856: Pull complete
56612e481988: Pull complete
101604b7d0fc: Pull complete
3534fc6dd93a: Pull complete
d8ef4bbd3669: Pull complete
644180171429: Pull complete
Digest: sha256:609e986496d5c95651aa14ffd2659beab2faa7d5e7e8153edc9cd4517c37345b
Status: Downloaded newer image for multiplatformautonomy/vscrimmage-vnc:latest
docker.io/multiplatformautonomy/vscrimmage-vnc:latest
</pre>
*Layer hashes and SHA256 digests in this documentation may not match current docker hub contents.*
