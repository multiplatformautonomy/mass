{ "title": "Licensing", "section": "mass" }
::meta::

# Licensing Summary

This document is a human-readable summary and is not a replacement for the licensing information found in the software distributions or code repositories.

## Software

|          **Software Product**         | **License** |
|:-------------------------------------:|:-----------:|
| Scenario Studio for SCRIMMAGE         |  Apache 2.0 |
| vSCRIMMAGE Docker Images              |         MIT |
| SCRIMMAGE                             |   LGPL v3.0 |
| Demonstration Code                    |         MIT |

## Documentation

The documentation on this website is available under the [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).
