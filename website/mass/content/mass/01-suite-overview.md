{ "title": "Overview", "section":"mass" }
::meta::
# Multi-platform Autonomy Simulation Suite (MASS)

## Suite Overview

### [SCRIMMAGE](https://github.com/gtri/scrimmage)
SCRIMMAGE is the autonomy modelling and simulation tool that provides the core functionality of MASS.  SCRIMMAGE is a stable tool that users can install separately from the rest of the suite, provided they are running Linux.  Users interested in using SCRIMMAGE without the other MASS tools should follow the instructions on http://scrimmagesim.org.


### [Virtualized SCRIMMAGE (vSCRIMMAGE)](https://gitlab.com/multiplatformautonomy/vscrimmage_vnc)
*v0.10.0 Beta*

Users who wish to run SCRIMMAGE on Windows or MacOS X can use Docker and two Virtualized SCRIMMAGE Docker Images.  These images are available on GitLab and Docker Hub.  SSS pulls the VNC image on startup.  For more information, see the documentation on the links below:<br/>
[vSCRIMMAGE Headless](https://gitlab.com/multiplatformautonomy/vscrimmage_headless)<br/>
[vSCRIMMAGE VNC](https://gitlab.com/multiplatformautonomy/vscrimmage_vnc)


### [Scenario Studio for SCRIMMAGE (SSS)](https://gitlab.com/multiplatformautonomy/scenario_studio_for_scrimmage)
*v0.10.0 Beta*

The Scenario Studio for SCRIMMAGE is a visual tool for developing and executing mission scenarios in SCRIMMAGE’s mission XML format.  It is available on Windows 10, MacOS X, and Linux.  Use the attached installers to install SSS with a docker-enabled virtual SCRIMMAGE backend.


### [Reinforcement Learning Demonstrations](https://gitlab.com/multiplatformautonomy/pytorch_demo)
*v0.10.0 Beta*

Users interested in using reinforcement learning with SCRIMMAGE via an OpenAI gym environment should look at the [pytorch_demo](https://gitlab.com/multiplatformautonomy/pytorch_demo) repository.  Currently, it contains a SCRIMMAGE OpenAI environment.  In the future, the MASS team plans to include RL starter code, including a demonstration of Deep RL autonomy in SCRIMMAGE using PyTorch and Tensorflow.


## Other Information

### [MASS User Group](https://gatech.co1.qualtrics.com/jfe/form/SV_egvjh6vmd0dLv7v)
Sign up for our MASS User Group slack channel!  Submit your name and email and we'll get you set up with an account.<br>
[MASS User Group Signup](https://gatech.co1.qualtrics.com/jfe/form/SV_egvjh6vmd0dLv7v)

### [GTRI](https://gtri.gatech.edu/)
MASS is supported by a cross-laboratory team of researchers and developers across multiple labs at Georgia Tech Research Institute (GTRI).

> The Georgia Tech Research Institute (GTRI) is the nonprofit, applied research division of the Georgia Institute of Technology (Georgia Tech). Founded in 1934 as the Engineering Experiment Station, GTRI has grown to more than 2,400 employees supporting eight laboratories in over 20 locations around the country, and performs more than $500 million of problem-solving research annually for government and industry.
**[About GTRI](https://gtri.gatech.edu/)**

[MASS Team Members](https://mass.gtri.gatech.edu/team)


### [Funding](https://www.wpafb.af.mil/afrl/)
This material is based on research sponsored by the Air Force Research Laboratory under Subaward number FA8650-19-2-6983. The U.S. Government is authorized to reproduce and distribute reprints for Governmental purposes notwithstanding any copyright notation thereon. The views and conclusions contained herein are those of the authors and should not be interpreted as necessarily representing the official policies or endorsements, either expressed or implied, of the Air Force Research Laboratory or the U.S. Government.