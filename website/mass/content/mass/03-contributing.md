{ "title": "Contributing" }
::meta::
# Contributing

## Contributions and Licensing
If you want to contribute code to the GTRI-maintained MASS repository on GitLab.com, please understand that it will be licensed the under the repository's applicable license for the general public and under an unlimited use license for U.S. Government Agencies.

Contributors whose changes are accepted can request in writing to be listed by name and GitLab username as a Community Contributor on our [Team page](/team).

## Code

Each piece of software under MASS has its own contributing guidlines which should be followed.  See the [MASS GitLab repositories](https://gitlab.com/multiplatformautonomy) for details.

## Documentation

You can contribute documentation to this website!  If you contribute changes to the documentation, please understand that it will be licensed under the Creative Commons Attribution 4.0 International license to the general public and under an unlimited use license for U.S. Government agencies.

Contributors whose documentation is accepted can request in writing to be listed by name and GitLab username as a Community Contributor on our [Team page](/team).