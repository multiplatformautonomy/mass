{ "title": "Mission Screen", "section":"sss" }
::meta::
# Mission Screen Overview

This editor provides an environment to initialize and configure SCRIMMAGE missions. From here, you can run the missions in a viewer, in batch runs, or train autonomy plugins.

## Plugins

### Built-in Plugins

Plugins provide a wide array of utilities for entities. A few examples of what plugins can do are: 
* Allow for Entities to communicate between each other.
* Have motion models for determining movement in simulation.
* Keep track of metrics for the number of collisions
* And much more! 

SCRIMMAGE contains several built-in plugins that can be used for simulations. A brief description of each built-in plugin is available in the [SCRIMMAGE docs](http://www.scrimmagesim.org/sphinx/html/overview/core-plugins.html?highlight=plugin). These plugins are available in SSS, and each entity can enable these plugins.

In order to enable a plugin in an Entity, it is as simple as clicking a button:
<img src="https://ucd3a0f7f319f7af99fd032ba1d0.previews.dropboxusercontent.com/p/orig/AA6resOCX2mgsc2lBchF71bH9_LnAs7ETj2Tivqe17YpOFXU5mJb0fS2lEFYzgBKnuAvxxEmWaO32Y-v5rZ2LV7FTNxshNfpn9h3qc4mslBCWcxBYjAZkUqnplSw85lyn2m_oQPYjxRT-DVfAqKy28Nad3uXPOklMWiC-DV3mt0xpMmjPlof1WDKJesuszraT9SPhbZiyVkQyssxOztp6lLl06Ts0uVo9HjZng--9OWMjPpQYNOlZUQh5N7giSMb8DYudoSAeWYiQuGkU4g3R6H2Ygx8q29t1cIJDA8K3dy8SrrQyg02WI0vIZYqqj25NmRfyjSvUhxlwas61SgAJKBi/p.gif?fv_content=true&size_mode=5"/>


Plugins are categorized according to their functionalities. Because some plugins provide overarching utility within the simulation, they only need to be enabled once. For instance, metric plugins that track the number of collisions with teammates would only need to be enabled in the Mission Entity in order to track all entities in the simulation. Other plugin types, such as autonomy plugins would be on a case-by-case basis for each Entity.

|      **Mission Entity**      |     **Entity**      |
|:----------------------------:|:-------------------:|
| Interaction                  |      Autonomy       |
| Metric                       |      Controller     |
| Network                      |      Motion Model   |
|                              |      Sensor         |



### Plugin Variables

Once a plugin is added to the Entity's property panel, the plugin's values can be tuned for the mission. This can be done by changing the values in the input box. Alternatively, variables can be used by clicking on the button next to the input box. This provides a dynamic method of setting values for plugin properties. 

TODO: Provide examples of variables and gif demoing the creation and selection of a variable.

### Custom Plugins

In addition to the built-in plugins provided, custom plugins can be created and distributed. The green addition button at the top of the Plugin List will prompt you to create and name a new plugin. An editor will open up inside of SSS with the boilerplate code needed for creating your own personal plugin. An example of creating an autonomy plugin is shown below:
<img src="https://uce436a0318e9d303b20ea93a7fc.previews.dropboxusercontent.com/p/orig/AA55cYacXaGJkalZ71oBuM7a5dZsBa5Xl44AnDTIwKn06kQNlSElbROO2k2tjuHJ2YeY9pr-il7QYsrUXOUUuU3V_n6CmDaIgqrIzyDnjJ_lksIeCQHdzLy3tFr_duNXrO7LIXEXZ9Ben-KMow3LNZHcoXEw1Lxq0c192LoBUngGf9T9RHeneCk98Ex1mr6tpjJQfqNtvVmppGvHH0m0HUEvxIjWpOxTwVFquyruT6Cp-YnD9U2wzXQ5YATxYXn0S703zofXWchY0r5wQ8gFQ8-a-Mx1vJeU2od3OZ9hlXc1h3f_8J-GdslFTDIhoICSaLo3_DANYxvQZCT6WGwlB1oF/p.gif?size=2048x1536&size_mode=3"/>

After creating the plugin, it will be saved in your project directory where you can reuse and distribute if you wish. The plugin will be added to the Plugin List, where you can enable it for each Mission Entity or Entity. For more information on how to create a customized plugin, please read the [SCRIMMAGE docs](http://www.scrimmagesim.org/sphinx/html/tutorials/autonomy-plugin.html).

## Configuration Settings

At the bottom of the sidebar, there is the configuration settings button. Clicking on the button will open a list of options to choose from as shown below:
<img src="/images/content/sss/ConfigurationButton.png"/>

### Git Commit

On startup of SSS, there is a prompt to setup Git information. The configuration setting to *commit any changes to git* will save the changes that you have made since your last commit. The commit dialog box will ask for you to state what changes have been made to the project since the last commit. These commits can later be pushed to a remote repository, for either distribution of a mission or for saving a backup of the project.
<img src="/images/content/sss/GitCommitDialogBox.png" />

### Select a Docker Image

There are multiple releases that have been made for SSS. These are saved as Docker images, and there may be a time where switching between these releases is necessary. SSS provides a setting so that the Docker image may be changed. A select box allows for choosing between images that are installed on your machine. Alternatively, the image can be typed in manually in the text box.
<img src="https://uc7f12240d404005f3fafc573926.previews.dropboxusercontent.com/p/orig/AA7B2e6HwbbkHdSKbZPI07_TagfmEUSn6gVKcKo9pG9VLDKLAizP2Ei3rf1cEnasN_WoPXtvtMtdhqUbaZWHr6euy-5i4vAM9GorsqIi9GZ0WW1GoCat8BlHEJ8PyIJUNamIjno9PSF4NxU5ccRsLS_BizMqlM7-iIlHgsmJ5j3Z3vsssW-rO_U3yud_6hw5B3ud1rxiPNjkJ_jxhbBwMtEL7ZguAzF28CQdDBYARFqL4uxCbaBvtcAwtvEeLfsQ_s9FH_0jypIdYz7C-TXN6A0yNX7GfUI_FRxXmaKM4ydROt3He5PB3H5BHfajN5Tp1norji0Geg_6k-y1MloIYx8o/p.gif?fv_content=true&size_mode=5"/>

## Run Mission Button

The last button in the sidebar provides the tools to run a mission in simulation, run in batch runs, or to write OpenAI scripts.
<img src="/images/content/sss/PlayMissionButton.png" />

### Batch Runs

To train and evaluate the efficiency of models, it is necessary to run mission in batch. Below is an example of running a mission 100 times in a batch locally, using four processors on the machine.
<img src="/images/content/sss/BatchSetupDialogBox.png" />

### Run OpenAI Scripts

[OpenAI Gym](https://openai.com/) is a toolkit for reinforcement learning. Clicking this button will open an editor with boilerplate code for starting up an OpenAI project. This OpenAI script is saved into the project directory. Once a script has been written, it can be tested out by running a single mission with the viewer.

### Run Single Mission with Viewer

Running this option will simulate your mission in a docker VNC. This utilizes a SCRIMMAGE simulation tool that allows you to view your mission at any speed that you wish (the speed can be set inside of the Mission Entity). This option is useful for debugging the mission and to assist in visualizing the scenario.
TODO: gif demo pressing play and it opening the simulation.