{ "title": "SSS Overview", "section":"sss" }
::meta::
# SSS Overview
## Scenario Studio for SCRIMMAGE (SSS) 
#### What is SSS used for?
SSS is a cross platform visual tool used to develop and execute mission scenarios in SCRIMMAGE. 
#### Why use it?
SSS allows users to use SCRIMMAGE on Windows 10, MacOS and Linux. It offers a user friendly UI with additional features that make SCRIMMAGE more accessible.


## SSS Capabilities
- Simplifies the mission design process by providing a list of mission entities and 3D mission editor interface. [Mission Screen](https://mass.gtri.gatech.edu/docs/sss/mission-screen)
- Allows users run scrimmage missions using a vSCRIMMAGE backend.
- Provides interface for managing mission-level variables and executing batch runs.
- Facilitates development SCRIMMAGE plugins with a built-in syntax-highlighting code editor.