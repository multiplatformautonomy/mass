{
    "title": "Title Screen"
}
::meta::
# TitleScreen
The Title Screen is the first screen that appears after launching the app. It is the starting point for launching the scrimmage software and setting up a scrimmage mission. From this screen, the user can initialize their git account settings, create or load a new mission, or bypass the SSS interface and work inside a Docker VNC container with the Scrimmage Software (advanced mode). 
![Title Screen Overview](/images/content/sss/TitleScreenOverview.png) 


## Git Settings 
The first step before loading or creating a new mission is to set up your git username and email using the Git Settings button on the title screen. Each mission created through SSS is initialized as a git repository. The git settings contains the account information that will be associated with the git repository and any git operations in your working directory. You can read more about git [here](https://git-scm.com/doc).

After entering your git account information and clicking submit, the git account information will be saved to application data and remembered upon future launches of the SSS application. This step only needs to be completed once for the initial launch of SSS.

**Note:**
If the git username and email are left blank this will cause an error to pop up when trying to create a mission. This is because the git credentials are necessary for initializing the git repository.

![Git Settings Dialog](/images/content/sss/GitSettingsDialog.png)


## Create New Mission Button
You can create a new mission with SSS by clicking the top button on the lefthand side of the screen. This will open a dialog where you can enter the name of the new project directory and select where to save it in your local file system.

![Create New Mission Dialog](/images/content/sss/CreateNewMissionDialog.png)

After selecting the 'Save Project' button in this dialog, a new directory will be saved in your local file system. SSS will then start up a new Docker container called sss_scrimmage and mount your directory to a folder called \missionMount within the sss_scrimmage container. It will also generate several files in your new project directory:
- **.git** - directory containing git information
- **.initialize.sh** - a script to setup vScrimmage
- **GeneratedMission.xml** - an XML document that is updated live to reflect the current state of the SSS mission. This is used in vScrimmage to run missions, and is used to load the mission back in.
- **ProjectPlugins** - a directory containing the source code and build files for the custom plugins that can be created on the Mission Screen

## Load Mission Button
The Load Project button allows you to open projects that were previously created with SSS. After clicking the Load Project button, a dialog will appear with two options for loading the mission:
- **Local Directory** - this selection will open up a file browser to select the project directory of a previous SSS mission on your local file system.
- **Git Clone** - this selection will prompt you for a link to a remote git repository and a location to clone it to on your local file system. 

After selecting your load option, SSS will start the sss_scrimmage container and mount the selected project to the \missionMount directory within the container. Then, SSS will switch to the Mission Screen which will be initialized with the data from your selected project's GeneratedMission.xml file.

![Load Mission Dialog](/images/content/sss/LoadOptionsDialog.png)

## Advanced User Mode
This button allows you to bypass the SSS interface and work directly within a vnc container with the Scrimmage software. It will open up a new window to the sss_scrimmage container where users can run Scrimmage through a command line interface. You can read more about the Scrimmage CLI [here](http://www.scrimmagesim.org/sphinx/html/index.html)

![VNC Screen](/images/content/sss/VncScreen.png)

## Code Editor
This button opens up the SSS Code Editor screen and can be used to modify the mission files directly. It also supports save, undo, redo, and delete operations.
![Code Editor Screen](/images/content/sss/CodeEditorScreen.png)
