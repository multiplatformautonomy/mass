//next.config.js
module.exports = {
    webpack: function(config) {
        config.module.rules.push({
            test: /\.static.html$/,
            use: 'raw-loader',
        });
        return config
    },
}