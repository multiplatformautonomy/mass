#!/bin/bash
if [ -z "$1" ]; then
    EXITVAL=0;
    for x in $(ls content/*.md content/*/*.md); do
        /bin/bash $0 $x;
        EV="$?"
        if [ $EV -ne 0 ]; then
            EXITVAL=1;
        fi
    done;
    exit $EXITVAL;
else
    echo "Linting $1";
    cat $1 |\
        perl -e '$x=0; while(<STDIN>){print if $x==1; $x=1 if /^::meta::$/;}' |\
        markdownlint -s
fi