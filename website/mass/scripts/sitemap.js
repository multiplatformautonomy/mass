var globby = require('globby');
var process = require('process')
var fs = require('fs');

let dir = process.argv[process.argv.length - 1];
if (fs.existsSync(dir)) {
    if(!fs.lstatSync(dir).isDirectory()) {
        console.error(`${dir} is not a directory.`);
        console.log(`Usage: yarn sitemap {output_directory}`);
        return 1;
    }
} else {
    console.error(`${dir} does not an existing directory.`);
    console.log(`Usage: yarn sitemap {output_directory}`);
    return 2;
}
let g = globby(dir, {
    expandDirectories: {
        extensions: ['html', 'jpg', 'png', 'ico']
    }
});
g.then((result) => {
    let sitemap = result.filter((file) => {
        return !(
            (file.indexOf('out/google') === 0) ||
            (file.indexOf('out/404') === 0)
        );
    }).map((file) => {
        return file
            .replace(/^out/,'https://mass.gtri.gatech.edu')
            .replace('/index.html','')
            .replace(/.html$/, '');
    }).join("\n");
    fs.writeFileSync(`${dir}/sitemap.txt`, sitemap);
});