#!/bin/bash
if [ -z "$1" ]; then
    EXITVAL=0;
    for x in $(ls content/*.md content/*.rst content/*/*.md content/*/*.rst); do
        /bin/bash $0 $x;
        EV="$?"
        if [ $EV -ne 0 ]; then
            EXITVAL=1;
        fi
    done;
    exit $EXITVAL;
else
    grep -qe '^::meta::$' $1 || exit 0;
    echo "Linting $1";
    cat $1 |\
        perl -e '$x=0; while(<STDIN>){$x=1 if /^::meta::$/; print if $x==0; }' |\
        jsonlint -q
fi