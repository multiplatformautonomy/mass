const serveIndex = require('serve-index');
const express = require('express');

const directory = './downloads';
const stylesheet = './index.css'

var app = express();
app.use('/', express.static(directory), serveIndex(directory, {
    icons: true,
    stylesheet
}));

app.listen(80);