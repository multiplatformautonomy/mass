const handler = require('serve-handler');
const http = require('http');
const publicDir = '../mass/out';
const publicPort = 8080;
const hostname = 'localhost';

const server = http.createServer((request, response) => {
  // You pass two more arguments for config and middleware
  // More details here: https://github.com/vercel/serve-handler#options
  console.log(`${request.method} ${request.url} from ${request.headers['user-agent']}`);
  return handler(request, response, {
    cleanUrls: true,
	  public: publicDir,
	  directoryListing: false
  });
})
 
server.listen(publicPort, () => {
  console.log(`Running at http://${hostname}${publicPort}`);
});
