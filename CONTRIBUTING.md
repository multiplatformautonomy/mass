# Contributing Documentation

## Process
1. Make a branch off of develop according to your issue #.
1. Make a new markdown (or restructured text) file under content corresponding to your documentation page.<br>If desired, you may prefix it with a two-digit number to determine the relative order of the documentation pages.
1. In the first line of your file, add the following json: `{ title: "My Cool Title Here" }`
1. After your json is done, add a new line with `::meta::`. This acts as a delimiter separating the json from the markdown/rst.
1. Write the rest of your markdown below.  For gifs and images, you can drop them directly into the content directory and they will be copied automatically to `/images/content/{dir}/filename.ext`.  For videos, discuss uploading them to a DropBox with Frazier and linking that way.
1. When you want to preview your changes, run `yarn content && yarn dev`.  This will generate the content from your markdown/rst into html and serve a local version of http://mass.gtri.gatech.edu on http://localhost:3000.
1. When you are ready, push your commits and submit a pull request to this repository.